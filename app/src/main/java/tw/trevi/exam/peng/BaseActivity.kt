package tw.trevi.exam.peng

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.FragmentActivity
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import tw.trevi.exam.peng.rx.DisposableInterface

@SuppressLint("Registered")
open class BaseActivity : FragmentActivity(), DisposableInterface {

    private val disposables = CompositeDisposable()
    override fun addDisposable(disposable: Disposable?) {
        disposables.add(disposable!!)
    }

    override fun removeDisposable(disposable: Disposable?) {
        disposables.remove(disposable!!)
    }

    public override fun onDestroy() {
        disposables.dispose()
        super.onDestroy()
    }
}