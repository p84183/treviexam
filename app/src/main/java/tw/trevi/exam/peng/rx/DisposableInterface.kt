package tw.trevi.exam.peng.rx

import io.reactivex.disposables.Disposable

interface DisposableInterface {
    fun addDisposable(disposable: Disposable?)
    fun removeDisposable(disposable: Disposable?)
}