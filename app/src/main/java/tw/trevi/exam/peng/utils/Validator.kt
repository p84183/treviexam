package tw.trevi.exam.peng.utils

import android.text.TextUtils
import android.util.Log

object Validator {
    private const val TAG = "Validator"

    class Result {
        var isValid = false
        var errorMessage = ""
    }

    fun validateIntRange(lower: Int, upper: Int, str: String): Result {
        val result = Result()
        if (TextUtils.isEmpty(str)) {
            result.errorMessage = "Can not be empty!"
            return result
        }

        val num: Int

        try {
            num = str.toInt()
        } catch (t: Throwable) {
            Log.e(TAG, "text \"$str\" parse to int error", t)
            result.errorMessage = "Parse error!"
            return result
        }

        when {
            num < lower -> {
                result.errorMessage = "Lower bound is $lower"
            }
            num > upper -> {
                result.errorMessage = "Upper bound is $upper"
            }
            else -> {
                result.isValid = true
            }
        }
        return result
    }
}