package tw.trevi.exam.peng

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.common.collect.ImmutableList
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_display.*
import kotlinx.android.synthetic.main.display_button.view.*
import kotlinx.android.synthetic.main.display_item.view.*
import tw.trevi.exam.peng.utils.Toasts
import java.util.concurrent.TimeUnit
import kotlin.random.Random


class DisplayActivity : BaseActivity() {
    companion object {
        const val TAG = "DisplayActivity"
        const val INTENT_FLAG_COLUMN = "INTENT_FLAG_COLUMN"
        const val INTENT_FLAG_ROW = "INTENT_FLAG_ROW"
        const val RANDOM_TIME_SECOND = 10L
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_display)

        val column = intent?.getIntExtra(INTENT_FLAG_COLUMN, 0)
        val row = intent?.getIntExtra(INTENT_FLAG_ROW, 0)

        Log.d(TAG, "column=$column, row=$row")

        if (column == null || row == null || column <= 0 || row <= 0) {
            Toasts.thereIsSomeThingWrong(this)
            finish()
            return
        }

        initUI(column, row)
    }

    var isPaused = false

    override fun onResume() {
        super.onResume()
        isPaused = false
    }

    override fun onPause() {
        isPaused = true
        super.onPause()
    }

    private fun initUI(column: Int, row: Int) {
        val itemAdapter = DisplayItemAdapter(column, row)
        displayItemRecyclerView.adapter = itemAdapter
        displayItemRecyclerView.layoutManager = GridLayoutManager(this, column)

        val buttonAdapter = DisplayButtonAdapter(column)
        buttonAdapter.onItemClickListener = object : DisplayButtonAdapter.OnItemClickListener {
            override fun onItemClick(position: Int) {
                if (buttonAdapter.activatedPosition == position) {
                    itemAdapter.clearActivatedItem()
                    buttonAdapter.clearActivatedItem()
                }
            }
        }
        displayButtonRecyclerView.adapter = buttonAdapter

        val random = Random(System.nanoTime())
        val d = Observable.timer(RANDOM_TIME_SECOND, TimeUnit.SECONDS)
            .repeat()
            .observeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (isPaused) {
                    return@subscribe
                }
                val x = random.nextInt(0, column) + 1
                val y = random.nextInt(0, row) + 1
                val msg = "x=$x, y=$y"
                Log.d(TAG, "[initUI] $msg")
                Toasts.lengthLong(this, msg)
                itemAdapter.setActivatedItem(column * (y - 1) + (x - 1))
                buttonAdapter.activatedPosition = x - 1
            }, {
                Log.e(TAG, "[initUI]", it)
            })

        addDisposable(d)
    }
}

class DisplayItemAdapter(private val column: Int, private val row: Int) : RecyclerView.Adapter<DisplayItemAdapter.ViewHolder>() {
    companion object {
        const val TAG = "DisplayAdapter"
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val viewHolder = ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.display_item, parent, false))
        val parentHeight = parent.height
        val parentWidth = parent.width
        val layoutParams = viewHolder.itemView.layoutParams
        layoutParams.height = parentHeight / row
        layoutParams.width = parentWidth / column
        viewHolder.itemView.layoutParams = layoutParams
        return viewHolder
    }

    private val itemList: List<Item> by lazy {
        val builder = ImmutableList.builder<Item>()
        val count = column * row

        for (i in 0 until count) {
            builder.add(Item(i))
        }
        builder.build()
    }

    private val colorList: List<Int> by lazy {
        val random = Random(System.nanoTime())
        val builder = ImmutableList.builder<Int>()

        for (i in 0 until row) {
            builder.add(random.nextInt())
        }
        builder.build()
    }

    override fun getItemCount(): Int = itemList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.isActivated = (activatedPosition != null) && (position % column == activatedPosition!! % column)
        holder.itemView.displayItemActivatedStatusTop.visibility = if (position / column == 0) View.VISIBLE else View.INVISIBLE
        holder.itemView.displayItemBackground.setBackgroundColor(colorList[position / column])
        holder.itemView.displayItemTextView.visibility = if (activatedPosition != null && position == activatedPosition) View.VISIBLE else View.INVISIBLE
    }

    private var activatedPosition: Int? = null

    fun setActivatedItem(position: Int?) {
        if (position == null || (position >= 0 && position < itemList.size)) {
            activatedPosition = position
            notifyDataSetChanged()
        } else {
            Log.e(TAG, "[setActivatedItem] itemList.size=${itemList.size} position=$position")
        }
    }

    fun clearActivatedItem() {
        activatedPosition = null
        notifyDataSetChanged()
    }

    class Item(val value: Int)
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}

class DisplayButtonAdapter(private val column: Int) : RecyclerView.Adapter<DisplayButtonAdapter.ViewHolder>() {

    interface OnItemClickListener {
        fun onItemClick(position: Int)
    }

    companion object {
        const val TAG = "DisplayAdapter"
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val viewHolder = ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.display_button, parent, false))
        val parentWidth = parent.width
        val layoutParams = viewHolder.itemView.layoutParams
        layoutParams.width = parentWidth / column
        viewHolder.itemView.layoutParams = layoutParams
        return viewHolder
    }

    private val itemList: List<Item> by lazy {
        val builder = ImmutableList.builder<Item>()
        for (i in 0 until column) {
            builder.add(Item(i))
        }
        builder.build()
    }

    override fun getItemCount(): Int = itemList.size

    var onItemClickListener: OnItemClickListener? = null

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.isActivated = (activatedPosition != null) && (position % column == activatedPosition)
        holder.itemView.displayItemActivatedStatusBottom.visibility = if (position / column == 0) View.VISIBLE else View.INVISIBLE
        holder.itemView.setOnClickListener {
            onItemClickListener?.onItemClick(position)
        }
    }

    var activatedPosition: Int? = null
        set(position) {
            if (position == null || (position >= 0 && position < itemList.size)) {
                field = position
                notifyDataSetChanged()
            } else {
                Log.e(TAG, "[setActivatedItem] itemList.size=${itemList.size} position=$position")
            }
        }

    fun clearActivatedItem() {
        activatedPosition = null
        notifyDataSetChanged()
    }

    class Item(val value: Int)
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}