package tw.trevi.exam.peng

import android.content.Intent
import android.os.Bundle
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import tw.trevi.exam.peng.utils.Validator


class MainActivity : AppCompatActivity() {
    companion object {
        const val TAG = "MainActivity"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        startButton.setOnClickListener {
            var shouldStart = true
            shouldStart = shouldStart && validateTextView(1, 15, columnEditText)
            shouldStart = shouldStart && validateTextView(1, 10, rowEditText)
            if (shouldStart) {
                val intent = Intent(this, DisplayActivity::class.java)
                val column = columnEditText.text.toString().toInt()
                val row = rowEditText.text.toString().toInt()
                intent.putExtra(DisplayActivity.INTENT_FLAG_COLUMN, column)
                intent.putExtra(DisplayActivity.INTENT_FLAG_ROW, row)
                startActivity(intent)
            }
        }
    }

    private fun validateTextView(lower: Int, upper: Int, editText: EditText): Boolean {
        val result = Validator.validateIntRange(lower, upper, editText.text.toString())
        if (result.isValid) {
            editText.error = null
        } else {
            editText.error = result.errorMessage
        }

        return result.isValid
    }
}
