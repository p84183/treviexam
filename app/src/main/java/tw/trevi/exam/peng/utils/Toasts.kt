package tw.trevi.exam.peng.utils

import android.content.Context
import android.view.Gravity
import android.widget.Toast
import androidx.annotation.StringRes
import tw.trevi.exam.peng.R
import java.lang.ref.WeakReference

object Toasts {
    private var toastRef = WeakReference<Toast>(null)

    fun lengthLong(context: Context, @StringRes res: Int) {
        lengthLong(context, context.getString(res))
    }

    fun lengthLong(context: Context, msg: String) {
        var toast = toastRef.get()
        toast?.cancel()

        toast = Toast.makeText(context, msg, Toast.LENGTH_LONG)
        toast.setGravity(Gravity.CENTER, 0, 0)
        toast.show()
        toastRef = WeakReference(toast)
    }

    fun thereIsSomeThingWrong(context: Context) {
        lengthLong(context, R.string.there_is_something_wrong)
    }
}
